[binaries]
c = 'armv7-unknown-linux-gnueabihf-cc'
cpp = 'armv7-unknown-linux-gnueabihf-c++'
ar = 'armv7-unknown-linux-gnueabihf-ar'
strip = 'armv7-unknown-linux-gnueabihf-strip'
pkgconfig = 'armv7-unknown-linux-gnueabihf-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'armv7'
endian = 'little'
