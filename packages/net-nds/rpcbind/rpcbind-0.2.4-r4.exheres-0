# Copyright 2010-2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'rpcbind-0.2.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require sourceforge systemd-service \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Universal Addresses to RPC Program Number Mapper"
DESCRIPTION="
The rpcbind utility is a server that converts RPC program numbers
into universal addresses.
"
HOMEPAGE+=" http://git.linux-nfs.org/?p=steved/${PN}.git"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~x86"
MYOPTIONS="systemd tcpd"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        net-libs/libtirpc
        tcpd? ( sys-apps/tcp-wrappers )
        !net-nds/portmap [[
            description = [ rpcbind replaces portmap ]
            resolution = uninstall-blocked-after
        ]]
        systemd? ( sys-apps/systemd )
    run:
        user/rpc
        group/rpc
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-0.2.4-runstatdir.patch
    "${FILES}"/${PN}-0.2.3-systemd-tmpfiles.patch
    "${FILES}"/${PN}-0.2.4-exherbo-environment.patch
    "${FILES}"/ee569be4d6189a68b38d2af162af00ff475b48e2.patch
    "${FILES}"/7ea36eeece56b59f98e469934e4c20b4da043346.patch
    "${FILES}"/c49a7ea639eb700823e174fd605bbbe183e229aa.patch
    "${FILES}"/c0e38c9fd1b2c6785af90c86b26a07724c2488e8.patch
    "${FILES}"/7c7590ad536c0e24bef790cb1e65702fc54db566.patch
    "${FILES}"/1e2ddd4ebd7a9266e6070f275fa35752752fdfd6.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --bindir=/usr/$(exhost --target)/bin
    --enable-warmstarts
    --with-nss-modules="files"
    --with-statedir=/run/${PN}
    --with-rpcuser=rpc
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "systemd systemdsystemunitdir ${SYSTEMDSYSTEMUNITDIR}"
    "systemd systemdtmpfilesdir /usr/$(exhost --target)/lib/tmpfiles.d"
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "tcpd libwrap"
)

src_install() {
    default

    insinto /etc/conf.d
    doins "${FILES}"/systemd/rpcbind.conf
}

