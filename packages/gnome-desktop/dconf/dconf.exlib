# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License

require bash-completion gnome.org [ suffix=tar.xz ]
require meson vala [ vala_dep=true ]
require gtk-icon-cache test-dbus-daemon
require gsettings

export_exlib_phases src_prepare src_test src_install

SUMMARY="A low-level configuration system"
HOMEPAGE="http://live.gnome.org/dconf"

DEFAULT_SRC_COMPILE_PARAMS=( AR="${AR}" )
MYOPTIONS="
    ( linguas:
        an ar as be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur gl he hi hu
        id it ja ko lt lv ml mr nb nl pa pl pt_BR pt ru sk sl sr@latin sr sv ta te tg th tr ug uk vi
        zh_CN zh_HK zh_TW
    )
"

DEFAULT_SRC_INSTALL_PARAMS=( completiondir=/ )

dconf_src_prepare() {
    meson_src_prepare

    # Fails due to unprefixed usage of nm. Not patched upstream for now
    # because they switched to meson.
    edo sed -e "s:^nm :$(exhost --tool-prefix)nm :" -i gsettings/abicheck.sh
}

dconf_src_test() {
    unset DISPLAY
    test-dbus-daemon_run-tests meson_src_test
}

dconf_src_install() {
    # gsettings_src_install uses `default`
    export GSETTINGS_DISABLE_SCHEMAS_COMPILE=1
    meson_src_install
    unset GSETTINGS_DISABLE_SCHEMAS_COMPILE

    hereenvd 80dconf-databases << EOF
CONFIG_PROTECT_MASK="/etc/dconf/db"
EOF

    dobashcompletion "${MESON_SOURCE}/bin/completion/dconf"
}

